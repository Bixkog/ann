//
// Created by Bixkog on 2015-07-17.
//

#ifndef ANN_NET_H
#define ANN_NET_H

#include <vector>
#include <exception>
#include <cmath>
#include "Node.h"

using Layer = std::vector<Node>;

class Net {
public:
    Net(const std::vector <int> &topology);
    void feedForward(const std::vector<double> &inputs);
    void backProp(const std::vector<double> &outputs);
    void write_data(const std::vector<double> &target_outputs);
    void write_data();
    std::vector<double> getOutputs() const;
private:
    std::vector<Layer> m_layers;
    double momentum;
    double learning_rate;
    void count_gradients(const std::vector<double> &outputs);
    double count_rms(const std::vector<double> &outputs);
};


#endif //ANN_NET_H
