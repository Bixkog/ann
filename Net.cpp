//
// Created by Bixkog on 2015-07-17.
//

#include "Net.h"

Net::Net(const std::vector<int> &topology) {
    int t_size = topology.size();
    if(t_size<3)
    	throw (std::exception());

    for(auto it = topology.begin();it!=topology.end();it++)
    {
        if(it==(topology.end()-1))//is it outputLayer?
        {//output
        	Layer new_layer;
        	for(int i=0;i<*it;i++)
        	{
        		Node n(0);
        		new_layer.push_back(n);
        	}
        	m_layers.push_back(new_layer);
    	}
        else
        {//input&hidden
        	Layer new_layer;
        	for(int i=0;i<*it;i++)
        	{
        		Node n(*(it+1));
        		new_layer.push_back(n);
        	}
        	Node bias(*(it+1));
        	bias.setInput(1.0);
        	new_layer.push_back(bias);
        	m_layers.push_back(new_layer);
    	}
    }

    momentum = 0.5;
    learning_rate = 0.5;
}

void Net::feedForward(const std::vector<double> &inputs)
{
	if(inputs.size()!=m_layers[0].size()-1)
		throw (std::exception());

	for(int i=0;i<inputs.size();i++)//input
		m_layers.at(0).at(i).setInput(inputs.at(i));


	for(int i = 1;i<m_layers.size()-1;i++)//hidden
	{
		Layer &previousLayer = m_layers[i-1];
		for(int j = 0;j<m_layers.at(i).size() - 1;j++)
		{
			//std::cout<<"Hidden "<<i<<" "<<j<<":"<<std::endl;
			m_layers[i][j].feedForward(previousLayer,j);
		}
	}

	for(int i=0;i<m_layers.back().size();i++)//output
	{
		//std::cout<<"Output "<<i<<": "<<std::endl;
		m_layers.back().at(i).feedForward(m_layers[m_layers.size()-2],i);
	}
}

double tanhderiv(double x)
{
	return 1.0-tanh(x)*tanh(x);
}

double Net::count_rms(const std::vector<double> &outputs)
{
	double e = 0;
	auto target = outputs.begin();
	for(Node &actual:m_layers.back())
	{
		double delta = *target - actual.getm_output();
		e+=delta*delta;
		target++;
	}
	return e/2;
}

void Net::count_gradients(const std::vector<double> &outputs)
{
	//output deltaError
	Layer &outputLayer = m_layers.back();
	for(int i = 0;i<outputs.size();i++)
		outputLayer[i].deltaError = outputLayer[i].m_output*
					   (1-outputLayer[i].m_output)*
					   (outputLayer[i].m_output - outputs[i]);			   
	//hidden deltaError
	for(int i = m_layers.size()-2;i>0;i--)
	{
		for(int j=0;j<m_layers.at(i).size()-1;j++)
		{ 
			double t_sum = 0;
			for(int k = 0;k<m_layers[i][j].outWeights.size();k++)
			{
				t_sum += m_layers[i][j].outWeights[k].weight*m_layers[i+1][k].deltaError;
			}
			m_layers[i][j].deltaError = t_sum*m_layers[i][j].m_output*(1-m_layers[i][j].m_output);
		}
	}
}



void Net::backProp(const std::vector<double> &outputs)
{
	if(outputs.size()!=m_layers.back().size())
		throw (std::exception());

	count_gradients(outputs);

	for(int i = m_layers.size()-2;i>=0;i--)
	{
		for(int j = 0;j<m_layers[i].size();j++)
		{
			Node &n = m_layers[i][j];
			for(int k = 0;k<n.outWeights.size();k++)
			{
				Connection &c = n.outWeights[k];
				double oldWeight = c.weight;
				c.weight += -learning_rate*m_layers[i+1][k].deltaError*n.m_output+momentum*c.deltaWeight;
				c.deltaWeight = c.weight - oldWeight;
				//std::cout<<c.deltaWeight<<std::endl;
			}
		}
	}

}



std::vector<double> Net::getOutputs() const
{
	std::vector<double> outputs;
	for(Node n:m_layers.back())
		outputs.push_back(n.getm_output());
	return outputs;
}

void Net::write_data(const std::vector<double> &target_outputs)
{
	std::cout<<"----------------------------------------"<<std::endl<<"Input: ";
	for(int i=0;i<m_layers[0].size()-1;i++)
	{
		std::cout<<m_layers[0][i].m_output<<" ";
	}
	std::cout<<std::endl<<"Actual output: ";
	for(int i=0;i<m_layers.back().size();i++)
	{
		std::cout<<m_layers.back()[i].m_output<<" ";
	}
	std::cout<<std::endl<<"Target output: ";
	for(double x:target_outputs)std::cout<<x<<" ";
	std::cout<<std::endl;
}

void Net::write_data()
{
	std::cout<<"----------------------------------------"<<std::endl<<"Input: ";
	for(int i=0;i<m_layers[0].size()-1;i++)
	{
		std::cout<<m_layers[0][i].m_output<<" ";
	}
	std::cout<<std::endl<<"Actual output: ";
	for(int i=0;i<m_layers.back().size();i++)
	{
		std::cout<<m_layers.back()[i].m_output<<" ";
	}
	std::cout<<std::endl;
}