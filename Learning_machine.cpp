#include "Learning_machine.h"

void Learning_machine::teach()
{
	while(!input.eof())
	{
		std::vector<double> inputs;
		double outputs;
		for(int i=0;i<2;i++)
		{
			int a;
			input>>a;
			inputs.push_back(a);
		}
		input>>outputs;
		std::vector<double> out = {outputs};
		net.feedForward(inputs);
		net.backProp(out);
		net.write_data(out);
	}
}

std::vector<double> Learning_machine::feed(const std::vector<double> &input)
{
	net.feedForward(input);
	net.write_data();
	return net.getOutputs();
}