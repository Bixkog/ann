//
// Created by Bixkog on 2015-07-17.
//

#include "Node.h"

double Node::randomWeight()
{
	static std::random_device rd;
	static std::uniform_real_distribution<double> urd(0.0,1.0);
	static std::default_random_engine re(rd());      
 	return urd(re);
}


Node::Node(int nextLayerSize)
{
	for(int i=0;i<nextLayerSize;i++)
		{
			outWeights.push_back(Connection(randomWeight()));
		}
}


double Node::getOutput(int pos) const
{
	//std::cout<<m_output<<" "<<outWeights.at(pos).weight<<std::endl;
	return m_output*outWeights.at(pos).weight;
}

double Node::getm_output() const
{
	return m_output;
}

double sigmoid(double x)
{
	return 1/(1+std::exp(-x));
}

void Node::feedForward(const Layer &prevLayer,int pos)
{
	double sum = 0.0;
	int i = 0;
	for(Node n : prevLayer)
	{
		//std::cout<<i++<<": ";
		sum+=n.getOutput(pos);
	}
	m_sum = sum;
	m_output = sigmoid(sum);
}

void Node::setInput(const double input)
{
	m_output = input;
}