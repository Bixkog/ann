#ifndef ANN_LEARNING_MACHINE
#define ANN_LEARNING_MACHINE

#include "Net.h"
#include <vector>
#include <fstream>
#include <string>


class Learning_machine
{
public:
	Learning_machine(const std::vector<int> &topology)
	:net(topology)
	{
		input.open("input.txt");
	}
	void teach();
	std::vector<double> feed(const std::vector<double> &input);

private:
	Net net;
	std::ifstream input;
};

#endif //ANN_LEARNING_MACHINE