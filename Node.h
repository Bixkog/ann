//
// Created by Bixkog on 2015-07-17.
//

#ifndef ANN_NODE_H
#define ANN_NODE_H

#include <iostream>
#include <vector>
#include <cmath>
#include <random>

class Node;

using Layer = std::vector<Node>;

struct Connection
{
	double weight;
	double deltaWeight;
	double gradient;
	Connection(double a):
					weight(a),
			   		deltaWeight(0),
			   		gradient(0){}
};

class Node {
public:
	Node(int nextLayerSize);
	double getOutput(int pos) const;
	double getm_output() const;
	void feedForward(const Layer &prevLayer,int pos);
	void setInput(const double input);
private:
	double randomWeight();
	double deltaError;

	double m_output;
	double m_sum;
	std::vector<Connection> outWeights;
	friend class Net;
	
};


#endif //ANN_NODE_H
